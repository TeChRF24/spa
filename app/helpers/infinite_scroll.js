import { PostsCard } from '../componets/PostCard.js';
import { SearchCard } from '../componets/SearchCard.js';
import { ajax } from './ajax.js';
import api from './wp_api.js'
export async function InfiniteScroll(){
    const d = document,
          w = window;
          //Obtenemos lo que typeo el usuario
    let query = localStorage.getItem('wpSearch'),
        apiURL,
        //Component va a guardar un componente
        Component;

    w.addEventListener('scroll',async function(){
//scrollTop, que tan separados estamos del top al hacer scroll. Toma como referencia que tan separado esta la parte superior visible del viewport a que tan separado esta de donde inicia el body
//ClientHeight, la altura total del viewport
//scrollHeight,. Es la altura total del scroll, toda la altura total que abarca el documento HTML
        let { scrollTop, clientHeight, scrollHeight } = d.documentElement,
            { hash } = w.location;
            console.log(scrollTop,clientHeight,scrollHeight);
//Que tan separado está la parte superior del viewport del inicio del documento + la altura del viewport
        if(scrollTop + clientHeight >= scrollHeight){
            api.page++;
//Si el hash viene vacio o si equivale a #/ estamos en el home y debe hacer lo que sigue
            if(!hash || hash === '#/'){
                apiURL = `${api.POSTS}&page=${api.page}`;
                Component = PostsCard;
            }else if(hash.includes('#/search')){
                apiURL = `${api.SEARCH}${query}&page=${api.page}`;
                Component = SearchCard;
            }else{
                return false;
            }
            //Cuando llegamos al final tambien carga el loader mientras trae más datos de la API
            d.querySelector('.loader').style.display = 'block';
            await ajax({
                url: apiURL,
                handleFetchDataWhenSucceded(posts){
                    //Cargando nuevos recursos cuando se haya llegado al final del scroll
                    let html = '';
                    posts.forEach(post => html += Component(post));
                    // document.querySelector('#main').innerHTML = document.querySelector('#main').innerHTML += html;

                    // insertAdjacent HTML/Element/Text
                    //Agregando justo antes de terminar el elemento HTML, justo después de su último elemento interno
                    d.getElementById('main').insertAdjacentHTML('beforeend',html);
                    d.querySelector('.loader').style.display = 'none';
                }
            })
        }
    })
}
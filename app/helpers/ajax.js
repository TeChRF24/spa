//Volvemos la función asincrona
export async function ajax(props){
    let { url, handleFetchDataWhenSucceded } = props;

    await fetch(url)
        .then(res => res.ok ? res.json() : Promise.reject(res))
        .then(json => handleFetchDataWhenSucceded(json))
        .catch(err => {
            let errorMessage = err.statusText || 'Ocurrió un error al acceder a la API';
            
            document.getElementById('main').innerHTML = `
                <div class='error'>
                    <p>Error ${err.status}: ${errorMessage}</p>
                </div>
            `;
            document.querySelector('.loader').style.display = 'none';
        })
}
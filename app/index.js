import api from './helpers/wp_api.js'
import {App} from './App.js'

document.addEventListener('DOMContentLoaded',App);
//agregamos un listener que escucha cuando el hash haya cambiado y vuelve a ejecutar App que es el componente principal
window.addEventListener('hashchange',function(){
    //Cada vez que cambie el hash va a cambiar a 1 de nuevo la variable page de api
    api.page = 1;
    App();
});
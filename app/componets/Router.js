import api from '../helpers/wp_api.js';
import {ajax} from '../helpers/ajax.js';
import { PostsCard } from './PostCard.js';
import { Post } from './Post.js';
import { SearchCard } from './SearchCard.js';
import { ContactForm } from './ContactForm.js';
//Convertimos el Router a una función asincrona para esperar por los datos en la función ajax y despues quirar el Loader, para que el Loader no se quite antes de tiempo
export async function Router(){
    const d = document, w = window;
    const $main = d.getElementById('main');
//extraemos el slug de location para saber en que ruta nos encontramos
    let { hash } = location;
    console.log(hash);
    $main.innerHTML = null;
    if(!hash || hash === '#/'){
        await ajax({
            url:api.POSTS,
            handleFetchDataWhenSucceded(posts){
                // console.log(posts);
                //Creamos variable html vacia para ir concatenando cada elemento que viene en string y pues unir todo lo resultante con innerHTML al componente padre Posts
                let html = '';
                posts.forEach(post => html += PostsCard(post));
                // d.querySelector('.loader').style.display = 'none';
                $main.innerHTML = html;
            }
        })
    }else if(hash.includes('#/search')){
        //Se obtiene lo que el usuario ingresó en la busqueda
        let query = localStorage.getItem('wpSearch');
        //Si no hay nada en la busqueda nos salimos del router y quitamos el loader
        if(!query){
            d.querySelector('.loader').style.display = 'none';
            return false;
        }
        await ajax({
            url: `${api.SEARCH}${query}`,
            handleFetchDataWhenSucceded(busqueda){
                console.log(busqueda);
                let html = "";
//Si la busqueda no contiene resultados muestra un mensaje de aviso instead
            if(busqueda.length===0){
                    html = `<p class="error">No existen resultados de busqueda para el término
                                <mark>${query}</mark>
                            </p>`
            }else{
                    busqueda.forEach((post) => (html += SearchCard(post)));
            }
                $main.innerHTML = html;
                // console.log(busqueda);
            }
        })
        // $main.innerHTML = '<h2>Sección del Buscador</h2>'
    }else if(hash === '#/contacto'){
        $main.innerHTML = '<h2>Sección de Contacto</h2>';
        $main.insertAdjacentElement('beforeend',ContactForm());
        // $main.appendChild(ContactForm());
    }else{
        // $main.innerHTML = '<h2>Aquí cargará el contenido del post previamente seleccionado</h2>';
        await ajax({
            url:`${api.POST}/${localStorage.getItem("wpPostId")}`,
            handleFetchDataWhenSucceded(post){
                console.log(post,'Post');
                $main.innerHTML += Post(post)
            }
        })
    }
    d.querySelector('.loader').style.display = 'none';
}
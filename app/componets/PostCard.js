export function PostsCard(props){
    let { date, id, slug, title, _embedded } = props;
    let dateFormat = new Date(date).toLocaleString(),
    urlPoster = _embedded['wp:featuredmedia'] ? _embedded['wp:featuredmedia'][0].source_url : 
    "https://placeimg.com/200/200/any";

    document.addEventListener('click', e=>{
//Comprobamos si se hizo click al elemento que coincide con el selector css que le pasamos al metodo matches
//Si retorna true entonce se le hizo click a ese elemeto y se procede con el codigo dentro del if
//El codigo del if verifica que se le haya dado click a un elemento diferente del enlace, entonces se retorna false y se detiene la ejecución del código en la funcion e=>
        if(!e.target.matches('.post-card a')) return false;
        localStorage.setItem('wpPostId',e.target.dataset.id);
    })

    return `
    <article class="post-card">
        <img src="${urlPoster}" alt="${title.rendered}">
        <h2>${title.rendered}</h2>
        <p>
            <time datetime="${date}">${dateFormat}</time>
            <a href="#/${slug}" data-id="${id}">Ver publicación</a>
        </p>
    </article>
    `
}
export function Main(){
    const $main = document.createElement('main');
    $main.id = 'main';
    if(!location.hash.includes('#/search')){
        $main.classList.add('grid-fluid');
    }
    if(location.hash.includes('#/contacto')){
        $main.classList.remove('grid-fluid');
    }
    return $main;
}
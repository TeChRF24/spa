export function SearchForm(){
    // return `<h2>Formulario de busqueda</h2>`;
    const $form = document.createElement('form'),
        $input = document.createElement('input');

        $form.classList.add('form-search');
        $input.name = 'search';
        $input.type = 'search';
        $input.placeholder = 'Buscar...';
        //Se desactiva el auto completado para que no de sugerencias de busqueda
        $input.autocomplete = "off";
        $form.appendChild($input);
//Para guardar la busqueda que hace el usuario, la obtenemos y la mostramos en la cajita donde se escribe (en el input de entrada)
        if(location.hash.includes('#/search')){
            $input.value = localStorage.getItem('wpSearch');
        }
//Si se aprieta el tachecito que tiene el input limpiamos el storage
        document.addEventListener('search',function(e){
            //Termina la ejecución de la función
            if(!e.target.matches('input[type="search"]')) return false;
//Si el elemento input no tiene nada escrito devuelve falso y lo cambiamos a true para limpiar el storage
            if(!e.target.value) localStorage.removeItem('wpSearch')
        })
        document.addEventListener('submit',function(e){
            if(!e.target.matches('.form-search')) return false;
            e.preventDefault();
            //Buscamos en el formulario el input con el atributo name llamado search y obtenemos su valor guardandolo en el localstorage
            localStorage.setItem('wpSearch',e.target.search.value);
            location.hash = `#/search?search=${e.target.search.value}`
        })

    return $form;
}
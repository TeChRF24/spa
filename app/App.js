// import api from './helpers/wp_api.js'
// import { ajax } from './helpers/ajax.js'
import { Loader } from './componets/Loader.js';
import { Header } from './componets/Header.js';
import { Main } from './componets/Main.js';
// import { PostsCard } from './componets/PostCard.js';
import { Router } from './componets/Router.js';
import { InfiniteScroll } from './helpers/infinite_scroll.js';
export function App(){
    // const d = document,
    const $root = document.getElementById('root');
    //Cada vez que haya un cambio en el slug se limpia todo el contenido anterior en $root que es el elemento principal
    $root.innerHTML = null;

    $root.appendChild(Header());
    $root.appendChild(Main());
    $root.appendChild(Loader());
    //Agregando el Router. Aquí se van a pintar nuestros elementos en pantasha
    Router();
    InfiniteScroll();
}





    // document.getElementById('root').innerHTML = `<h1>Single Page App Test</h1>`;